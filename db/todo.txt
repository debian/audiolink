AudioLink

$Id: todo.txt,v 1.1 2003/09/13 10:24:04 amitshah Exp $

Todo for the mysql.schema:

1. It currently accepts NULL values for a lot of things. Correct this.

2. Set the pathname field to a unique one. This way, duplication of
   entries will be avoided.

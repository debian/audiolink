# AudioLink
#
# $Id: Makefile,v 1.24 2003/12/06 14:57:40 amitshah Exp $
#
# Copyright (C) 2003, Amit Shah <amitshah@gmx.net>
#
# This file is part of AudioLink.
#
# AudioLink is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 2 of the License, or
# (at your option) any later version.
#
# AudioLink is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with AudioLink; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

#------------------------------------------------------------------------------
# Developer section. Don't modify this section unless you know what
# you are doing. Look for user-modifiable settings in the user section
# below.

# versioning info
VERSION = 0
SUBLEVEL = 05
EXTRAVERSION =

ALVER = $(VERSION).$(SUBLEVEL)$(EXTRAVERSION)

DESTDIR =
BIN = $(DESTDIR)/usr/bin

# We'll install the man pages in the 1st section of man pages
MANEXT = 1
MAN = $(DESTDIR)/usr/share/man/man$(MANEXT)

POD2HTML_OPTS = --header --backlink Top --cachedir=/tmp

# run as make <target> Q= on the command-line to see the commands
# being run.
ifeq ($(Q),)
	Q = @
else
	Q = " "
endif

clean:
	$(Q)find . -name '*~' -o -name '.#*' -o -name '*.tmp' | xargs rm -f

version:
	$(Q)echo "AudioLink version " $(ALVER) `date +%Y%m%d` > VERSION

# Make tarballs
dist:  html_doc version clean
	$(Q)echo "Building packages for AudioLink" $(ALVER)
	$(Q)cp -r ../audiolink ../audiolink-$(ALVER)
	$(Q)find ../audiolink-$(ALVER) -name 'CVS*' | xargs rm -rf
	$(Q)rm -rf ../audiolink-$(ALVER)/patches
	$(Q)rm -rf ../audiolink-$(ALVER)/ann
	$(Q)rm -rf ../audiolink-$(ALVER)/web
	$(Q)rm -rf ../audiolink-$(ALVER)/debian
	$(Q)rm -f ../audiolink-$(ALVER)/.cvsignore
	$(Q)rm -f ../audiolink-$(ALVER)/web/upd_web.sh

	$(Q)tar --directory=.. -cjf ../audiolink-$(ALVER).tar.bz2 audiolink-$(ALVER)
	$(Q)tar --directory=.. -czf ../audiolink-$(ALVER).tar.gz audiolink-$(ALVER)
	$(Q)md5sum ../audiolink-$(ALVER).tar.* > ../md5sums
	$(Q)cp ../audiolink-$(ALVER).tar.gz ../aldeb/audiolink_$(ALVER).orig.tar.gz
	$(Q)rm -rf ../audiolink-$(ALVER)
	$(Q)echo "Done. Have you run 'make cvs' yet?"

# Make sure directories exist before installing
prepare_install:
	install -d $(BIN) $(MAN)

# Install it
install: prepare_install man
	$(Q)install code/alfilldb $(BIN)
	$(Q)install code/alsearch $(BIN)
	$(Q)install code/audiolink $(BIN)

# Generate HTML documentation
html_doc:
	$(Q)pod2html $(POD2HTML_OPTS) --infile code/alfilldb --outfile Documentation/alfilldb_doc.html
	$(Q)pod2html $(POD2HTML_OPTS) --infile code/alsearch --outfile Documentation/alsearch_doc.html
	$(Q)pod2html $(POD2HTML_OPTS) --infile code/audiolink --outfile Documentation/audiolink_doc.html

# Generate and install man pages
man:
	$(Q)pod2man --section=$(MANEXT) code/alfilldb $(MAN)/alfilldb.$(MANEXT)
	$(Q)pod2man --section=$(MANEXT) code/alsearch $(MAN)/alsearch.$(MANEXT)
	$(Q)pod2man --section=$(MANEXT) code/audiolink $(MAN)/audiolink.$(MANEXT)

# Commit latest and tag the CVS with the release info
cvs: version
	$(Q)cvs ci -m "version $(ALVER)" VERSION
	$(Q)cvs tag rel_$(VERSION)$(SUBLEVEL)

#------------------------------------------------------------------------------
# User section starts
#
# Modify the values below if you want to tweak your database
# settings (eg., the name of the database, the name of the table, etc.

DATABASE = aldb
DBI_USER = amit # fill in your username (for the db) here.
DBI_PASS = pass # fill in your password (for the db) here.
DBI_HOST = localhost # fill in the name of the machine running mysql

DB_OPTS = -u$(DBI_USER) -p$(DBI_PASS) -h$(DBI_HOST)

.PHONY: db createdb insschema

# This target is used to get to the mysql prompt. Users will rarely
# need to execute this target.
db:
	$(Q)mysql $(DB_OPTS) $(DATABASE)

# The next two targets are used to initialize and setup the database
# for running audiolink. They should be used just once: when the users
# are running audiolink for the first time. A script is in works which
# will handle all of this, so that the setting up is less pain for
# users.

createdb:
	$(Q)mysqladmin $(DB_OPTS) create $(DATABASE)

insschema:
	$(Q)mysql $(DB_OPTS) $(DATABASE) < db/mysql.schema
